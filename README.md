# Mini_Project_6_React/Flask

## Các Tính Năng
- Liệt kê danh sách bài viết
- Thêm, sửa, xóa bài viết
- Login người dùng để thực hiện thêm sửa, xóa, bài viết

## Cách Sử Dụng
(yêu cầu có sẵn [Docker](https://www.docker.com/), [docker-compose](https://docs.docker.com/compose/) và [npm](https://pypi.org/project/npm/) trong máy)

- Clone project
```bash
git clone https://gitlab.com/ducminhtong2k3/tf_bai6_angular-flask.git
```
- Mở terminal, cd vào folder frontend
```bash
cd tf_bai6_react-flask
cd frontend
```
- Tải node_modules
```bash
npm install
```
- Chạy project trên docker-compose
```bash
docker-compose up --build
```

