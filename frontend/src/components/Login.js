import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom';
import authService from '../services/auth.services';

function Login() {

    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(null);

    const navigate = useNavigate()

    const handleLogin = async (e) => {
        e.preventDefault();
        try {
            await authService.login(name, password)
                .then(
                    () => {
                        navigate('/');
                    },
                );
        }
        catch (err) {
            setError(err.response.data);
        }
    }

    return (
        <div>
            <form onSubmit={handleLogin}>
                <h1>Login</h1>
                <input
                    type='text'
                    placeholder='name'
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    required
                />
                <input
                    type='password'
                    placeholder='password'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
                <button type='submit'>Submit</button>
                {error && <h3>{error}</h3>}
            </form>
            <br/>
            <h4>Don't have account? <Link to={'/signup'}>Signup</Link></h4>
        </div>
    )
}

export default Login