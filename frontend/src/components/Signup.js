import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import authService from '../services/auth.services';

function Signup() {

    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(null);

    const navigate = useNavigate();


    const handleSignup = async (e) => {
        e.preventDefault();
        try {
            await authService.signup(name, password)
                .then(
                    () => {
                        navigate('/login');
                    },
                );
        }
        catch (err) {
            setError(err.response.data);  
        }
    }


    return (
        <div>
            <form onSubmit={handleSignup}>
                <h1>Signup</h1>
                <input
                    type='text'
                    placeholder='name'
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    required
                />
                <input
                    type='password'
                    placeholder='password'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
                <button type='submit'>Submit</button>
                {error && 
                    <h3>{error}</h3>
                }
            </form>
        </div>
    )
}

export default Signup