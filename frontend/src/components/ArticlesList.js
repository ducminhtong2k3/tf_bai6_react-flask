import React, { useEffect, useState } from 'react';
import ReactPaginate from "https://cdn.skypack.dev/react-paginate@7.1.3";
import { Link, useNavigate } from 'react-router-dom';
import articleService from '../services/article.services';

function ArticlesList() {

    const [articles, setArticles] = useState([]);
    const [isLoggedIn, setIsLoggedIn] = useState(null);
    const [articleOffset, setArticleOffset] = useState(0);
    const [currentArticles, setCurrentArticles] = useState(null);
    const [pageCount, setPageCount] = useState(0);

    const navigate = useNavigate();

    const Delete = async (article) => {
        try {
            await articleService.DeleteArticle(article.id)
                .then(() => {
                    window.location.reload();
                });
        }
        catch (err) {
            alert('You need to Login');
            localStorage.clear();
            navigate('/login');
        }
    }

    const Logout = () => {
        localStorage.clear();
        navigate('/login');
    }

    useEffect(() => {

        const endOffset = articleOffset + 4;

        fetch(`http://localhost:5000/get`, {
            'method': 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(res => setArticles(res))
            .catch(err => console.log(err))

        setCurrentArticles(articles.slice(articleOffset, endOffset));
        setPageCount(Math.ceil(articles.length / 4));

        if (localStorage.getItem('user') != null) {
            const user = JSON.parse(localStorage.getItem('user'));
            setIsLoggedIn(user.username);
        }
        else {
            setIsLoggedIn(null);
        }

    }, [articles.length, articleOffset]);

    const handlePageClick = (event) => {
        const newOffset = (event.selected * 4) % articles.length;
        setArticleOffset(newOffset);
    };


    return (
        <div className='container py-4'>
            <nav className="navbar navbar-expand navbar-dark bg-dark" aria-label="Second navbar example">
                <div className="container-fluid">
                    <h1 className="navbar-brand">ArticlesList</h1>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarsExample02">
                        <ul className="navbar-nav me-auto">
                            <li className="nav-item">
                                <Link className="nav-link active" aria-current="page" to={'/'}>Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link active" aria-current="page" to={'/login'}>Login</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link active" aria-current="page" to={'/signup'}>Signup</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link active" aria-current="page" to={'/create'}>Create</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            {currentArticles && currentArticles.map(article => {
                return (
                    <div key={article.id}>
                        <h2>{article.title}</h2>
                        <p>{article.context}</p>
                        <p>{article.date}</p>

                        <button className='btn btn-dark'><Link className="nav-link active" aria-current="page" to={'/update'} state={article}>Update</Link></button>
                        <button className='btn btn-light' onClick={() => Delete(article)}>Delete</button>

                    </div>
                )
            })}
            <br/>
            <footer>
                <ReactPaginate
                    nextLabel="next"
                    onPageChange={handlePageClick}
                    pageRangeDisplayed={3}
                    marginPagesDisplayed={2}
                    pageCount={pageCount}
                    previousLabel="previous"
                    pageClassName="page-item"
                    pageLinkClassName="page-link btn"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakLabel="..."
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName="active"
                />
            </footer>

            {isLoggedIn && (<h3 className='float-end'>User: {isLoggedIn}</h3>)}
            {isLoggedIn && (<button className='float-end btn btn-primary' onClick={Logout}>Logout</button>)}

        </div>
    )
}

export default ArticlesList;