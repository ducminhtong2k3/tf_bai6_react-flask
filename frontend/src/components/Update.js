import React, { useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom';
import articleService from '../services/article.services';

function Update() {

    const location = useLocation();
    const article = location.state;

    const [title, setTitle] = useState(article.title);
    const [context, setContext] = useState(article.context);

    const navigate = useNavigate();

    const handleUpdate = async (e) => {
        e.preventDefault();
        try {
            await articleService.UpdateArticle(article.id, { title, context })
                .then(() => {
                    navigate('/');
                });
        }
        catch (err) {
            alert("You need to login");
            localStorage.clear();
            navigate('/login');
        }
    }

    return (
        <div>
            <label htmlFor='title' className='form-label'>Title</label>
            <input
                type='text'
                className='form-control'
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                required
            />

            <label htmlFor='context' className='form-label'>Context</label>
            <textarea
                rows='3'
                type='text'
                className='form-control'
                value={context}
                onChange={(e) => setContext(e.target.value)}
                required
            />

            <button onClick={handleUpdate}>Update</button>
        </div>
    )
}

export default Update