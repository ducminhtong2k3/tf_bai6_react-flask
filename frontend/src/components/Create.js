import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import articleService from '../services/article.services';

function Create() {

    const [title, setTitle] = useState('');
    const [context, setContext] = useState('');

    const navigate = useNavigate()

    const handleCreate = async (e) => {
        e.preventDefault();
        try {
            await articleService.CreateArticle({title, context})
                .then(() => {
                    navigate('/');
                });
        }
        catch (err) {
            alert('You need to login first');
            localStorage.clear();
            navigate('/login');
        }
    }

    return (
        <div>
            <label htmlFor='title' className='form-label'>Title</label>
            <input
                type="text"
                className="form-control"
                placeholder='Please enter title'
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                required
            />

            <label htmlFor='context' className='form-label'>Context</label>
            <textarea
                rows='3'
                type="text"
                className="form-control"
                placeholder='Please enter context'
                value={context}
                onChange={(e) => setContext(e.target.value)}
                required
            />

            <button onClick={handleCreate}>Create</button>
        </div>
    )
}

export default Create