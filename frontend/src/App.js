import { Route, Routes } from 'react-router-dom';
import './App.css';
import ArticlesList from './components/ArticlesList';
import Create from './components/Create';
import Login from './components/Login';
import Signup from './components/Signup';
import Update from './components/Update';


function App() {

  return (

    <Routes>
      <Route path='/' element={<ArticlesList />} />
      <Route path="/login" element={<Login />} />
      <Route path="/signup" element={<Signup />} />
      <Route path='/create' element={<Create />} />
      <Route path='/update' element={<Update />} />
    </Routes>

  );
}

export default App;
