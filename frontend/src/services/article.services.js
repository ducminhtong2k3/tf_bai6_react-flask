import authHeader from "./auth-header"


export default class articleService {
    static UpdateArticle(id, body) {
        return fetch(`http://localhost:5000/update/${id}`, {
            'method': 'PUT',
            headers: authHeader(),
            body: JSON.stringify(body)
        })
        .then(res => res.json())
    }

    static CreateArticle(body) {
        return fetch(`http://localhost:5000/add`, {
            'method': 'POST',
            headers: authHeader(),
            body: JSON.stringify(body)
        })
        .then(res => res.json())
    }

    static DeleteArticle(id) {
        return fetch(`http://localhost:5000/delete/${id}`, {
            'method': 'DELETE',
            headers: authHeader(),
        })
        .then(res => res.json())
    }
}