import axios from "axios";


const signup = (name, password) => {
    return axios
        .post(`http://localhost:5000/signup`, {
            name, password
        })
        .then(res => {
            if (res.data.token) {
                localStorage.setItem('user', JSON.stringify(res.data));
            }
            return res.data;
        });
};


const login = (name, password) => {
    return axios
        .post(`http://localhost:5000/login`, {
            name, password
        })
        .then(res => {
            if (res.data.token) {
                localStorage.setItem('user', JSON.stringify(res.data));
            }
            return res.data;
        });
};

const logout = () => {
    localStorage.clear()
}

const authService = {
    login, signup, logout,
}

export default authService;