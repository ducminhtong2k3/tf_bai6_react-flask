import jwt
from flask import Flask, jsonify, request, make_response
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from datetime import datetime, timedelta
from werkzeug.security import check_password_hash, generate_password_hash
from functools import wraps
import uuid

app = Flask(__name__)

app.config.from_pyfile('config.py')

db = SQLAlchemy(app)

ma = Marshmallow(app)

CORS(app)


class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text())
    context = db.Column(db.Text())
    date = db.Column(db.DateTime, default=datetime.now())

    def __init__(self, title, context):
        self.title = title
        self.context = context


class ArticleSchema(ma.Schema):
    class Meta:
        fields = ('id', 'title', 'context', 'date')


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text())
    password = db.Column(db.Text())
    public_id = db.Column(db.Integer)

    def __init__(self, name, password, public_id):
        self.name = name
        self.password = password
        self.public_id = public_id


class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name')


article_schema = ArticleSchema()
articles_schema = ArticleSchema(many=True)

user_schema = UserSchema()


def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None
        if 'Authorization' in request.headers:
            token = request.headers['Authorization']

        if not token:
            return make_response('need token', 401, {'Authentication': 'Login required'})

        try:
            data = jwt.decode(token, "minh", algorithms=["HS256"])
            current_user = User.query.filter_by(public_id=data['public_id']).first()
        except:
            return make_response('token is invalid', 401, {'Authentication': 'Login required'})

        return f(current_user, *args, **kwargs)
    return decorator


@app.route('/get', methods=['GET', ])
def get_articles():
    all_articles = Article.query.order_by(Article.id.desc()).all()
    res = articles_schema.dump(all_articles)
    return jsonify(res)


@app.route('/get/<int:id>', methods=['GET', ])
def post_details(id):
    article = Article.query.get(id)
    return article_schema.jsonify(article)


@app.route('/add', methods=['POST', ])
@token_required
def add_article(current_user):
    title = request.json['title']
    context = request.json['context']

    articles = Article(title=title, context=context)
    db.session.add(articles)
    db.session.commit()
    return article_schema.jsonify(articles)


@app.route('/update/<int:id>', methods=['PUT', ])
@token_required
def update_article(current_user, id):
    article = Article.query.get(id)

    title = request.json['title']
    context = request.json['context']

    article.title = title
    article.context = context

    db.session.commit()
    return article_schema.jsonify(article)


@app.route('/delete/<int:id>', methods=['DELETE', ])
@token_required
def delete_article(current_user, id):
    article = Article.query.get(id)
    db.session.delete(article)
    db.session.commit()
    return article_schema.jsonify(article)


@app.route('/signup', methods=['POST', ])
def signup():
    name = request.json['name']
    password = request.json['password']

    user = User.query.filter_by(name=name).first()

    if user:
        return make_response('user already exists', 403)

    new_user = User(name=name, password=generate_password_hash(password), public_id=str(uuid.uuid4()))

    db.session.add(new_user)
    db.session.commit()
    return user_schema.jsonify(new_user)


@app.route('/login', methods=['POST', ])
def login():
    name = request.json['name']
    password = request.json['password']

    user = User.query.filter_by(name=name).first()

    if user is None:
        return make_response('username incorrect', 403, {'Authentication': 'Login required'})

    if check_password_hash(user.password, password):
        # Generate JWT
        token = jwt.encode(
            {'public_id': user.public_id, 'exp': datetime.utcnow() + timedelta(seconds=60)},
            "minh", "HS256"
        )
        return jsonify({'token': token, 'username': user.name})
    return make_response('password incorrect', 403)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
